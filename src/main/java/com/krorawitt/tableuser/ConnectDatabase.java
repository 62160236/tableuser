/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.krorawitt.tableuser;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author WINDOWS 10
 */
public class ConnectDatabase {

    public static void main(String[] args) {
        Connection conn = null;
        String dbName = "user.db";
        
        try {
            Class.forName("org.sqlite.JDBC");
            conn = DriverManager.getConnection("jdbc:sqlite:user.db" + dbName);
        } catch (ClassNotFoundException ex) {
            System.exit(0);
        } catch (SQLException ex) {
            System.out.println("Unable open database!!");
            System.exit(0);
        }
        System.out.println("Opened database successfully");
    }
}
